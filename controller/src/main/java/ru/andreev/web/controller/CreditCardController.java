package ru.andreev.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.andreev.CreditCardService;
import ru.andreev.models.CreditCard;

@Controller
public class CreditCardController {

    @Autowired
    private CreditCardService creditCardService;

    @GetMapping("/creditCards")
    public String getAllCreditCards(Model model) {
        Iterable<CreditCard> creditCards = creditCardService.findAllCreditCards();
        model.addAttribute("creditCards", creditCards);

        return "creditCards";
    }

    @GetMapping("/editCreditCard")
    public String editCreditCard(@RequestParam("id") Long id, Model model) {
        CreditCard creditCard = creditCardService.findCreditCard(id).get();
        model.addAttribute("creditCard", creditCard);

        return "editCreditCard";
    }

    @GetMapping("/deleteCreditCard")
    public String deleteCreditCard(@RequestParam("id") Long id) {
        creditCardService.deleteCreditCard(id);

        return "redirect: creditCards";
    }

    @GetMapping("/addCreditCard")
    public String addCreditCard(Model model) {
        model.addAttribute("creditCard", new CreditCard());

        return "addCreditCard";
    }

    @PostMapping("/saveCreditCard")
    public String saveCreditCard(@ModelAttribute CreditCard creditCard) {
        creditCardService.saveCreditCard(creditCard);

        return "redirect: creditCards";
    }
}
