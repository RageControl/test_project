package ru.andreev.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.andreev.ProductService;
import ru.andreev.models.Product;

import java.util.Optional;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public String getAllProducts(Model model) {
        Iterable<Product> products = productService.findAllProducts();
        model.addAttribute("products", products);

        return "products";
    }

    @GetMapping("/editProduct")
    public String editProduct(@RequestParam("id") Long id, Model model) {
        Optional<Product> product = productService.findProduct(id);
        model.addAttribute("product", product);

        return "editProduct";
    }

    @GetMapping("/deleteProduct")
    public String deleteProduct(@RequestParam("id") Long id) {
        productService.deleteProduct(id);

        return "redirect: products";
    }

    @GetMapping("/addProduct")
    public String addProduct(Model model) {
        model.addAttribute("product", new Product());

        return "addProduct";
    }

    @PostMapping("/saveProduct")
    public String saveProduct(@ModelAttribute Product product) {
        productService.saveProduct(product);

        return "redirect: products";
    }
}
