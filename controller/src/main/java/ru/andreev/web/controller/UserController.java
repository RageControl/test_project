package ru.andreev.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.andreev.CreditCardService;
import ru.andreev.UserService;
import ru.andreev.models.CreditCard;
import ru.andreev.models.User;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CreditCardService creditCardService;

    @GetMapping("/users")
    public String getAllUsers(Model model) {
        Iterable<User> users = userService.findAllUsers();
        model.addAttribute("users", users);

        return "users";
    }

    @GetMapping("/editUser")
    public String editUser(@RequestParam("id") Long id, Model model) {
        User user = userService.findUser(id).get();
        model.addAttribute("user", user);

        return "editUser";
    }

    @GetMapping("/deleteUser")
    public String deleteUser(@RequestParam("id") Long id) {
        userService.deleteUser(id);

        return "redirect: users";
    }

    @GetMapping("/addUser")
    public String addUser(Model model) {
        model.addAttribute("user", new User());

        return "addUser";
    }

    @PostMapping("/saveUser")
    public String saveUser(@ModelAttribute("user") User user) {
        CreditCard card = creditCardService.findCreditCard(user.getCreditCard().getId()).get();
        user.setCreditCard(card);
        userService.saveUser(user);

        return "redirect: users";
    }

}
