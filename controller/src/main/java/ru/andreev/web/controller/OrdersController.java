package ru.andreev.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.andreev.OrdersService;
import ru.andreev.ProductService;
import ru.andreev.UserService;
import ru.andreev.models.Orders;
import ru.andreev.models.Product;
import ru.andreev.models.User;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

@Controller
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @GetMapping("/orders")
    public String getAllOrders(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam("dateFrom") LocalDate dateFrom, Model model) {
        Iterable<Orders> orders;
        if (dateFrom.equals(null)) {
            orders = ordersService.findAllOrders();
        } else {
            orders = ordersService.getOrdersFromDate(dateFrom);
        }
        BigDecimal fullCost = ordersService.getCostFromDate(dateFrom);
        model.addAttribute("orders", orders);
        model.addAttribute("fullCost", fullCost);

        return "orders";
    }

    @GetMapping("/addOrder")
    public String addOrder(Model model) {
        model.addAttribute("orders", new Orders());
        model.addAttribute("allProducts", productService.findAllProducts());

        return "addOrder";
    }

    @PostMapping("/saveOrder")
    public String saveOrder(@ModelAttribute Orders orders) {
        User user = userService.findUser(orders.getUser().getId()).get();
        Collection<Product> products = new ArrayList<>();
        Iterator<Product> iterator = orders.getProducts().iterator();
        while (iterator.hasNext()) {
            products.add(productService.findProduct(iterator.next().getId()).get());
        }
        orders.setUser(user);
        orders.setProducts(products);
        ordersService.saveOrder(orders);

        return "redirect: orders?dateFrom=2017-12-12";
    }

    @GetMapping("/getOrdersFromDate")
    public String getOrdersFromDate(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam("dateFrom") LocalDate dateFrom, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Model model) {
        model.addAttribute("dateFrom", dateFrom);

        return "redirect: orders?dateFrom=" + dateFrom;
    }
}
