package ru.andreev.web.config;

import org.springframework.format.Formatter;
import ru.andreev.models.Product;

import java.text.ParseException;
import java.util.Locale;

public class ProductFormatter implements Formatter<Product> {
    @Override
    public Product parse(String id, Locale locale) throws ParseException {
        Product product = new Product();
        product.setId(Long.valueOf(id));
        return product;
    }

    @Override
    public String print(Product product, Locale locale) {
        return String.valueOf(product.getId());
    }
}
