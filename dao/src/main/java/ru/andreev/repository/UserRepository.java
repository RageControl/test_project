package ru.andreev.repository;

import org.springframework.data.repository.CrudRepository;
import ru.andreev.models.User;

public interface UserRepository extends CrudRepository<User, Long> {
}
