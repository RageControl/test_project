package ru.andreev.repository;

import org.springframework.data.repository.CrudRepository;
import ru.andreev.models.CreditCard;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
}
