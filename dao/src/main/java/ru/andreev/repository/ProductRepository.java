package ru.andreev.repository;

import org.springframework.data.repository.CrudRepository;
import ru.andreev.models.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
