package ru.andreev.repository;

import org.springframework.data.repository.CrudRepository;
import ru.andreev.models.Orders;

public interface OrdersRepository extends CrudRepository<Orders, Long> {
}
