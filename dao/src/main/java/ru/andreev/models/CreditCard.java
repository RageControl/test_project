package ru.andreev.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "credit_card")
public class CreditCard {
    @Id
    @GeneratedValue
    @Column(name = "credit_card_id")
    private long id;
    private String number;
    private int month;
    private int year;
    private String code;
    @OneToOne(mappedBy = "creditCard")
    private User user;
}
