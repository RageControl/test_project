package ru.andreev.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;

@Data
@Entity
public class Product {
    @Id
    @GeneratedValue
    @Column(name = "product_id")
    private long id;
    private String name;
    private BigDecimal price = BigDecimal.valueOf(0);
    @Column(name = "is_adult")
    private int isAdult;
    @ManyToMany(mappedBy = "products")
    private Collection<Orders> ordersCollection;

    public BigDecimal getPrice() {
        return price.setScale(2, BigDecimal.ROUND_DOWN);
    }

    public void setPrice(BigDecimal price) {
        this.price = price.setScale(2, BigDecimal.ROUND_DOWN);
    }
}
