package ru.andreev.models;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;

@Data
@Entity
public class User {
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    private long id;
    private String firstname;
    private String lastname;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
    private BigDecimal balance = BigDecimal.valueOf(0);
    private String login;
    private String password;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "credit_card_id", referencedColumnName = "credit_card_id")
    private CreditCard creditCard;
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<Orders> orders;


    public BigDecimal getBalance() {
        return balance.setScale(2, BigDecimal.ROUND_DOWN);
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance.setScale(2, BigDecimal.ROUND_DOWN);
    }
}
