package ru.andreev.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import ru.andreev.models.Orders;
import ru.andreev.models.User;
import ru.andreev.repository.OrdersRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class OrdersRepositoryTest {

    private static Orders orders = new Orders();
    private static Collection<Orders> ordersCollection = new LinkedList<>();

    @Mock
    OrdersRepository orderRepositoryMock;

    @BeforeAll
    public static void init() {
        orders.setDate(LocalDate.of(2020, 1, 2));
        orders.setCost(BigDecimal.valueOf(12.32));
        orders.setUser(new User());
        ordersCollection.add(orders);
    }

    @Test
    public void creditCardSaveTest() {
        when(orderRepositoryMock.save(orders)).thenReturn(orders);
        Assertions.assertEquals(orders, orderRepositoryMock.save(orders));
    }

    @Test
    public void creditCardUpdateTest() {
        orders.setCost(BigDecimal.valueOf(32.12));
        when(orderRepositoryMock.save(orders)).thenReturn(orders);
        Assertions.assertEquals(orders, orderRepositoryMock.save(orders));
    }

    @Test
    public void creditCardRemoveTest() {
        orderRepositoryMock.deleteById(orders.getId());
        verify(orderRepositoryMock).deleteById(orders.getId());
    }

    @Test
    public void creditCardFindTest() {
        when(orderRepositoryMock.findById(anyLong())).thenReturn(Optional.of(orders));
        Assertions.assertEquals(Optional.of(orders), orderRepositoryMock.findById(Long.valueOf(0)));
    }

    @Test
    public void creditCardFindAllTest() {
        when(orderRepositoryMock.findAll()).thenReturn(ordersCollection);
        Assertions.assertEquals(ordersCollection, orderRepositoryMock.findAll());
    }
}
