package ru.andreev.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import ru.andreev.models.CreditCard;
import ru.andreev.repository.CreditCardRepository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CreditCardRepositoryTest {

    private static CreditCard card = new CreditCard();
    private static Collection<CreditCard> cards = new LinkedList<>();

    @Mock
    CreditCardRepository creditCardRepositoryMock;


    @BeforeAll
    public static void init() {
        card.setNumber("6666666666666666");
        card.setMonth(1);
        card.setYear(20);
        card.setCode("12kjnva24");
        cards.add(card);
    }


    @Test
    public void creditCardSaveTest() {
        when(creditCardRepositoryMock.save(card)).thenReturn(card);
        Assertions.assertEquals(card, creditCardRepositoryMock.save(card));
    }

    @Test
    public void creditCardUpdateTest() {
        card.setNumber("9999999999999999");
        when(creditCardRepositoryMock.save(card)).thenReturn(card);
        Assertions.assertEquals(card, creditCardRepositoryMock.save(card));
    }

    @Test
    public void creditCardRemoveTest() {
        creditCardRepositoryMock.deleteById(card.getId());
        verify(creditCardRepositoryMock).deleteById(card.getId());
    }

    @Test
    public void creditCardFindTest() {
        when(creditCardRepositoryMock.findById(anyLong())).thenReturn(Optional.of(card));
        Assertions.assertEquals(Optional.of(card), creditCardRepositoryMock.findById(Long.valueOf(0)));
    }

    @Test
    public void creditCardFindAllTest() {
        when(creditCardRepositoryMock.findAll()).thenReturn(cards);
        Assertions.assertEquals(cards, creditCardRepositoryMock.findAll());
    }

}
