package ru.andreev.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import ru.andreev.models.Product;
import ru.andreev.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ProductRepositoryTest {

    private static Product product = new Product();
    private static Collection<Product> products = new LinkedList<>();

    @Mock
    ProductRepository productRepositoryMock;

    @BeforeAll
    public static void init() {
        product.setName("name");
        product.setPrice(BigDecimal.valueOf(43.23));
        products.add(product);
    }

    @Test
    public void productSaveTest() {
        when(productRepositoryMock.save(product)).thenReturn(product);
        Assertions.assertEquals(product, productRepositoryMock.save(product));
    }

    @Test
    public void productUpdateTest() {
        product.setPrice(BigDecimal.valueOf(23.43));
        when(productRepositoryMock.save(product)).thenReturn(product);
        Assertions.assertEquals(product, productRepositoryMock.save(product));
    }

    @Test
    public void productRemoveTest() {
        productRepositoryMock.deleteById(product.getId());
        verify(productRepositoryMock).deleteById(product.getId());
    }

    @Test
    public void productFindTest() {
        when(productRepositoryMock.findById(anyLong())).thenReturn(Optional.of(product));
        Assertions.assertEquals(Optional.of(product), productRepositoryMock.findById(Long.valueOf(0)));
    }

    @Test
    public void productFindAllTest() {
        when(productRepositoryMock.findAll()).thenReturn(products);
        Assertions.assertEquals(products, productRepositoryMock.findAll());
    }
}
