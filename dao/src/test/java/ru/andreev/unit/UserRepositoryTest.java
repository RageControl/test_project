package ru.andreev.unit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import ru.andreev.models.CreditCard;
import ru.andreev.models.User;
import ru.andreev.repository.UserRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserRepositoryTest {

    private static User user = new User();
    private static Collection<User> users = new LinkedList<>();

    @Mock
    UserRepository userRepositoryMock;

    @BeforeAll
    public static void init() {
        user.setFirstname("firstname");
        user.setLastname("lastname");
        user.setBirthday(LocalDate.of(1992, 12, 12));
        user.setBalance(BigDecimal.valueOf(322.2));
        user.setLogin("login");
        user.setPassword("password");
        user.setCreditCard(new CreditCard());
        users.add(user);
    }

    @Test
    public void userSaveTest() {
        when(userRepositoryMock.save(user)).thenReturn(user);
        Assertions.assertEquals(user, userRepositoryMock.save(user));
    }

    @Test
    public void userUpdateTest() {
        user.setBalance(BigDecimal.valueOf(322));
        when(userRepositoryMock.save(user)).thenReturn(user);
        Assertions.assertEquals(user, userRepositoryMock.save(user));
    }

    @Test
    public void userRemoveTest() {
        userRepositoryMock.deleteById(user.getId());
        verify(userRepositoryMock).deleteById(user.getId());
    }

    @Test
    public void userFindTest() {
        when(userRepositoryMock.findById(anyLong())).thenReturn(Optional.of(user));
        Assertions.assertEquals(Optional.of(user), userRepositoryMock.findById(Long.valueOf(0)));
    }

    @Test
    public void userFindAllTest() {
        when(userRepositoryMock.findAll()).thenReturn(users);
        Assertions.assertEquals(users, userRepositoryMock.findAll());
    }
}
