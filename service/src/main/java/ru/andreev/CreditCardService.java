package ru.andreev;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.andreev.models.CreditCard;
import ru.andreev.repository.CreditCardRepository;

import java.util.Optional;

@Service
@Transactional
public class CreditCardService {
    @Autowired
    private CreditCardRepository creditCardRepository;

    public Optional<CreditCard> findCreditCard(Long id) {
        return creditCardRepository.findById(id);
    }

    public Iterable<CreditCard> findAllCreditCards() {
        return creditCardRepository.findAll();
    }

    public void deleteCreditCard(Long id) {
        creditCardRepository.deleteById(id);
    }

    public CreditCard saveCreditCard(CreditCard creditCard) {
        return creditCardRepository.save(creditCard);
    }
}
