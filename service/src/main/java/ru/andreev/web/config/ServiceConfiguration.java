package ru.andreev.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.andreev.CreditCardService;
import ru.andreev.OrdersService;
import ru.andreev.ProductService;
import ru.andreev.UserService;

@Configuration
public class ServiceConfiguration {
    @Bean
    public CreditCardService getCreditCardService() {
        return new CreditCardService();
    }

    @Bean
    public OrdersService getOrderService() {
        return new OrdersService();
    }

    @Bean
    public ProductService getProductService() {
        return new ProductService();
    }

    @Bean
    public UserService getUserService() {
        return new UserService();
    }
}
