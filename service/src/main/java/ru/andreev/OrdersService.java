package ru.andreev;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.andreev.models.Orders;
import ru.andreev.repository.OrdersRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrdersService {
    @Autowired
    private OrdersRepository orderRepository;

    public Optional<Orders> findOrder(Long id) {
        return orderRepository.findById(id);
    }

    public Iterable<Orders> findAllOrders() {
        return orderRepository.findAll();
    }

    public void deleteOrder(Long id) {
        orderRepository.deleteById(id);
    }

    public Orders saveOrder(Orders orders) {
        return orderRepository.save(orders);
    }

    public Collection<Orders> getOrdersFromDate(LocalDate date) {
        Collection <Orders> orders = new ArrayList<>();
        Iterable<Orders> ordersIterable = findAllOrders();
        for (Orders orders1:ordersIterable){
            orders.add(orders1);
        }

        return orders
                .stream()
                .filter(order -> order.getDate()
                        .isAfter(date))
                .collect(Collectors.toList());
    }

    public BigDecimal getCostFromDate(LocalDate date) {
        return getOrdersFromDate(date)
                .stream()
                .map(Orders::getCost)
                .reduce(BigDecimal::add)
                .get();
    }
}
